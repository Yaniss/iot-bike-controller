import React, { Component } from 'react';
import axios from 'axios';
import { Image, View, Text, TouchableHighlight } from 'react-native';
import { mapping } from '@eva-design/eva';
import { dark as darkTheme } from '@eva-design/eva';
import { ApplicationProvider, Layout, Button } from 'react-native-ui-kitten';
import {executeFunction} from '../features/features';
import {
    SCLAlert,
    SCLAlertButton
  } from 'react-native-scl-alert';

import { 
    Ionicons
  } from '@expo/vector-icons';



const logo = require ('./bycicleLogo.png');
const alertLogo = require ('../../assets/alarm.png');
const lightOff = require ('../../assets/lightOff.png');
const lightOn = require ('../../assets/lightOn.png');
const lock = require ('../../assets/lock.png');
const unlock = require ('../../assets/unlock.png');
const position = require ('../../assets/position.png');

const features = [
    [
        {
            icon : lock,
            nameFunc : 'lock',
            text : 'Lock',
            color:"warning",
        },
        {
            icon : unlock,
            nameFunc : 'unlock',
            text : 'Unlock',
            color:"success",
        },
    ],
    [
        {
            icon : lightOn,
            nameFunc : 'lightsOn',
            text : 'On',
            color:"primary",
        },
        {
            icon : lightOff,
            nameFunc : 'lightsOff',
            text : 'Off',
            color:"basic",
        },
    ],
    [
        {
            icon : position,
            nameFunc : 'position',
            text : 'Pos.',
            color:"info",
        },
        {
            icon : alertLogo,
            nameFunc : 'alarm',
            text : 'Alarm',
            color:"danger",
        }
    ]
]
export default class Home extends Component {
    state = {
        navigate: undefined,
        showErr: false,
        alarm : false,
    }

    componentDidMount(){
        const {navigate} = this.props.navigation;
        this.setState({
            navigate : navigate
        })
    }

    handleClose = () => {
        this.setState({ showErr: false })
    }
    onPressLogo = async () => {
        await axios.get('https://bike-controller.herokuapp.com/ringtone');
    }

    onPressButtons = async (item) => {
        let alarm;
        if (item.nameFunc === 'alarm') {
            alarm = (this.state.alarm === false ? true : false);
            this.setState({
                ...this.state,
                alarm: alarm
            })
        }
        let error = await executeFunction(item.nameFunc, this.state.navigate, alarm);
        if (error === true) {
            this.setState({ showErr: true })
        }
    }


    render () {
        return (
            <ApplicationProvider mapping={mapping} theme={darkTheme}>
                <SCLAlert
                    style={{marginTop:'-60%'}}
                    show={this.state.showErr}
                    onRequestClose={this.handleClose}
                    theme="danger"
                    title="Oops \n"
                    subtitle="Error has been occured \n"
                    headerIconComponent={<Ionicons name="ios-warning" size={32} color="white" />}
                />
                <Layout style={{height:'100%'}}>
                    <View style={{marginTop: "30%", alignItems: 'center'}}>
                        <TouchableHighlight onPress={this.onPressLogo}>
                            <Image
                                style={{width: 200, height: 200}}
                                source={logo}
                            />
                        </TouchableHighlight>
                    </View>

                    <View style={{flex: 3.4, flexDirection: 'column', alignItems: 'center'}}>
                    {features.map((array, index) => (
                        <View key={index} style={{flexDirection: 'row'}} >
                            {array.map((item, key) => (
                                <View key={key} style={{alignItems: 'center', padding: 20}}>
                                    <Button status={item.color} size='giant' style={{width:150, height:100}}
                                        onPress={() => this.onPressButtons(item)}
                                    >
                                        <Text>
                                        {"\n"}
                                            <Image
                                                style={{width: 30, height: 30}}
                                                source={item.icon}
                                            />
                                        </Text>
                                        <Text >
                                            {item.text} {"\n"}
                                        </Text>
                                    </Button>
                                </View>
                            ))}
                        </View>
                    ))}
                    </View>
                </Layout>
            </ApplicationProvider>
        )
    }
};