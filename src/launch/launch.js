import React, { Component } from 'react';
import { Image, Dimensions } from 'react-native';
import { ApplicationProvider, Layout } from 'react-native-ui-kitten';
import ProgressBarAnimated from 'react-native-progress-bar-animated';



const logo = require ('./bycicleLogo.png');

export default class Home extends Component {
    state = {
        progress:20,
        navigate : undefined,
        timeout: setTimeout(()=> {this.state.navigate('Home')}, 2000),
        progressBarTO : setTimeout(() => this.progressBar(), 1000)
    }

    componentDidMount(){
        const {navigate} = this.props.navigation;
        this.setState({
            navigate: navigate
        })
        this.progressBar();
    }

    progressBar() {
        progress = Math.random() * 40 + this.state.progress;
        if (progress > 100)
            progress = 100;
        this.setState({ 
            progress: progress
        })
        if (this.state.progress === 100) {
            clearTimeout(this.state.timeout());
            clearTimeout(this.state.progressBarTO());
        }
    }

    render () {
        const barWidth = Dimensions.get('screen').width - 30;
        return (
            <Layout style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                <Image
                    style={{width: 200, height: 200, alignItems: 'center'}}
                    source={logo}
                />
                 <ProgressBarAnimated
                    width={barWidth}
                    value={this.state.progress}
                    backgroundColor="#71dec7"
                    backgroundColorOnComplete="#6CC644"
                />
            </Layout>
        )
    }
};