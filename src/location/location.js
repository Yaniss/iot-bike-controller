import React, { Component } from 'react';
import MapView, {Marker} from 'react-native-maps';
import {
    ActivityIndicator,
    StyleSheet,
    View,
  } from 'react-native'
import axios from 'axios';


const styles = StyleSheet.create({
    container: {
      position: 'absolute',
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
      justifyContent: 'flex-end',
      alignItems: 'center',
    },
    containerLoading: {
        flex: 1,
        justifyContent: 'center'
      },
    map: {
      position: 'absolute',
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
    },
  })

export default class Home extends Component {
    state = {
        longitude: undefined,
        latitude: undefined,
        ready : false
    }

    componentDidMount () {
        axios.get('https://bike-controller.herokuapp.com/location').then((res) => {
            this.setState({
                latitude:res.data.location.latitude,
                longitude:res.data.location.longitude,
                ready: true
            })
        })
    }
    render() {
        return (
            <View style={styles.container}>
                {this.state.ready ? 
                    <MapView
                        style={styles.map}
                        initialRegion={{
                            latitude: this.state.latitude,
                            longitude: this.state.longitude,
                            latitudeDelta: 0.0922,
                            longitudeDelta: 0.0421,
                        }}
                        >
                            <Marker title='Bike GPS' description='Your bike is located there'
                            coordinate={{latitude: this.state.latitude, longitude: this.state.longitude}} />
                    </MapView>
                :
                    <View style={[styles.containerLoading]}>
                        <ActivityIndicator size="large" color="#71dec7"/>
                    </View>
                }
            </View>
        )
    }
};