import  { alarmOn, alarmOff } from './alarm';
import  { lightsOn, lightsOff } from './lights';
import  { unlock, lock } from './locker';

export const executeFunction = async (feature, navigate, alarm = undefined) => {
    let error = false;
    switch (feature) {
        case 'lock':
            console.log("lock");
            error = await lock();
            break;
        case 'unlock':  
            console.log('unlock');
            error = await unlock();
            break;
        case 'lightsOn':
            console.log('light On');
            error = await lightsOn();
            break;
        case 'lightsOff':
            console.log('light Off');
            error = await lightsOff();
            break;
        case 'position':
            console.log('position');
            navigate('Location');
            break;
        case 'alarm':
            if (alarm === 'on') {
                error = await alarmOn();
            } else {
                error = await alarmOff();
            }
            break;
    }
    return error;
}