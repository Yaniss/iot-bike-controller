import axios from 'axios';
import Toast from 'react-native-root-toast';

export const alarmOn = async () => {
    let error = false;
    await axios.post('https://bike-controller.herokuapp.com/alarm', { alarmStatus: 'on' })
    .then((res) => {
        Toast.show('Alarm is on', {
            position: 80,
            shadow: true,
            animation: true,
            hideOnPress: true,
            delay: 3,
            backgroundColor: "#71dec7",
            shadow: true
        });
    }).catch((err) => {
        console.log(err);
        error = true;
    })
    return (error);
}

export const alarmOff = async () => {
    let error = false;
    await axios.post('https://bike-controller.herokuapp.com/alarm', { alarmStatus: 'off' })
    .then((res) => {
        Toast.show('Alarm is off', {
            position: 80,
            shadow: true,
            animation: true,
            hideOnPress: true,
            delay: 3,
            backgroundColor: "#71dec7",
            shadow: true
        });
    }).catch((err) => {
        console.log(err);
        error = true;  
    })
    return (error);
}