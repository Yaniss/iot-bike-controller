import axios from 'axios';
import Toast from 'react-native-root-toast';

export const lock = async () => {
    let error = false;
    await axios.post('https://bike-controller.herokuapp.com/locker', { lockStatus: 'lock' })
    .then((res) => {
        Toast.show('The bike is locked', {
            position: 80,
            shadow: true,
            animation: true,
            hideOnPress: true,
            delay: 3,
            backgroundColor: "#71dec7",
            shadow: true
        });
    }).catch((err) => {
        console.log(err);
        error = true;
    })
    return (error);
}

export const unlock = async () => {
    let error = false;
    await axios.post('https://bike-controller.herokuapp.com/locker', { lockStatus: 'unlock' })
    .then((res) => {
        Toast.show('The bike is unlocked', {
            position: 80,
            shadow: true,
            animation: true,
            hideOnPress: true,
            delay: 3,
            backgroundColor: "#71dec7",
            shadow: true
        });
    }).catch((err) => {
        console.log(err);
        error = true;
    })
    return (error);
}