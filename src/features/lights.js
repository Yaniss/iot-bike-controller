import axios from 'axios';
import Toast from 'react-native-root-toast';

export const lightsOn = async () => {
    let error = false;
    await axios.post('https://bike-controller.herokuapp.com/lights', { lightsStatus: 'on' })
    .then((res) => {
        Toast.show('The lights are turned on', {
            position: 80,
            shadow: true,
            animation: true,
            hideOnPress: true,
            delay: 3,
            backgroundColor: "#71dec7",
            shadow: true
        });
    }).catch((err) => {
        console.log(err);
        error = true;
    })
    return (error);
}

export const lightsOff = async () => {
    let error = false;
    await axios.post('https://bike-controller.herokuapp.com/lights', { lightsStatus: 'off' })
    .then((res) => {
        Toast.show('The lights are turned off', {
            position: 80,
            shadow: true,
            animation: true,
            hideOnPress: true,
            delay: 3,
            backgroundColor: "#71dec7",
            shadow: true
        });
    }).catch((err) => {
        console.log(err);
        error = true;
    })
    return (error);
}