import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import Home from '../src/home/home';
import Launch from '../src/launch/launch';
import Location from '../src/location/location';

const MainNav = createStackNavigator({
    Launch: {screen: Launch},
    Home: {screen: Home},
    Location: {screen: Location},
}, {headerMode: 'none'});

const App = createAppContainer(MainNav);

export default App;  
