import React from 'react';
import { StyleSheet } from 'react-native';
import { ApplicationProvider } from 'react-native-ui-kitten';
import { mapping } from '@eva-design/eva';
import { dark as darkTheme } from '@eva-design/eva';
import AppNavigator from './navigation/AppNavigator';

export default function App() {
  return (
    <ApplicationProvider mapping={mapping} theme={darkTheme}>
      <AppNavigator />
    </ApplicationProvider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
