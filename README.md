# IoT APP - Bike Controller

## Getting started

### Prerequisites
    Be sure to have nodejs and npm installed on the machine

### Clone the project
    - git@gitlab.com:Yaniss/iot-bike-controller.git
    
### Run the app
    - npm install
    - npm start

### App

<img src="assets/screenshot_app.jpg"  width="500" height="900">
<img src="assets/screenshot_map.jpg"  width="500" height="900">

